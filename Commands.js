
Cypress.Commands.add('getToken', (user, passwd)=>{
    cy.request({
        method: 'POST',
        url: '/signin',
        body: {
          email: user,
          redirecionar: false,
          senha: passwd
        }
    }).its('body.token').should('not.be.empty')
    .then(token =>{
       // Cypress.env('token', token)
        return token
    })
})


Cypress.Commands.add('resetRest', ()=>{
    cy.getToken('raulacacio@hotmail.com', '123456').then(token =>{
        cy.request({
            method: 'GET',
            url:'/reset',
            headers:{ Authorization: `JWT ${token}`}
        }).its('status').should('be.equal', 200);
    })
   
})

Cypress.Commands.add('verificaID', name =>{
    cy.getToken('raulacacio@hotmail.com', '123456').then(token =>{
    cy.request({
        method: 'GET',
        url: '/contas',
        headers:{ Authorization: `JWT ${token}`},
        qs:{ 
            nome: name
        }
    }).then(res =>{
        return res.body[0].id
    })
 })
})

//Esse Command foi uma tentariva frustada de otimizar o código e implementar o token dentro do Header
//a cada chamada do request, mais tarde eu volto  te resolvo e deixo atualizado no Gitlab
/*
Cypress.Commands.overwrite('request', (originalFn, options) => {
    if(options.length === 1){
        if(Cypress.env('token')){
            console.log(originalFn)
            options[0].headers = {
                Authorization: `JWT ${Cypress.env('token')}`,
            }
        }
    }
    return originalFn(options)
})*/