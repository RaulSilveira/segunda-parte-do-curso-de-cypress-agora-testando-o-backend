/*Segunda parte do curso feito na Udemy usando o  Framework Cypress
agora testado o backend da aplicação */ 

/*Desta vez deixando o teste mais otimizado, Usando dentro o arquivo 
Cypress.json a URl base e dentro do arquivo Commans.js alguns comando de otimização*/

const { select, reject } = require("async");

//Abrindo o describe e começando o teste
describe('Teste do backend', ()=>{
    //Váriavel global do token
    let token;

    //usando um Before para ter o token a cada it
 before(()=>{
     //passando os pârametros de email e senha 
    cy.getToken( 'raulacacio@hotmail.com', '123456')
    .then(tkn =>{
        token = tkn;
    })
 })
 
 //Aqui foi usado um beforeEach para resete a cada execução dos its
 beforeEach(()=>{
    cy.resetRest()
 })

 //Criando conteúdo e verificando o status code da aplicação
 it('conteúdo e status',()=>{
 

    //Aqui o request já está usando uma URL base, assim podendo passar somente a rota necessária
    cy.request({
        url: '/contas', 
        method: 'POST',
        headers:{ Authorization: `JWT ${token}`},
         body: {
            nome: 'Conta via rest'
         }
     }).as('response')
  
  cy.get('@response').then(res =>{
      //Verificando se o status code é de fato acertivo
      expect(res.status).to.be.equal(201);
      //Verifica se a propriedade é um ID
      expect(res.body).to.have.property('id')
      //verifica se a conta criada foi a mesma passada no body acima
      expect(res.body).to.have.property('nome', 'Conta via rest')
  })
 })

 //Alterando o nome de uma conta do HTML através do request e verificando o status code acertivo
 it('Alterando o nome', ()=>{
    cy.verificaID('Conta para alterar')
     .then(contaId => {
        cy.request({
            url: `/contas/${contaId}`,
            method: 'PUT',
           headers:{ Authorization: `JWT ${token}`},
            body:{
                nome: 'Conta alterada via rest'
            }
   
        }).as('response')
        //verificando se o status corresponde a 200 como é esperado
        cy.get('@response').its('status').should('be.equal', 200)
     }) 
     
 })
     //Esse teste tenta inserir uma conta repetida para verificar qual o status code que será passado
it('Conta repetida', ()=>{
    cy.request({
        url: '/contas', 
        method: 'POST',
        headers:{ Authorization: `JWT ${token}`},
         body: {
            nome: 'Conta mesmo nome'
         },
         failOnStatusCode: false
     }).as('response')
  
  cy.get('@response').then(res =>{

    //verifica se o status code é da casa do 4 ou sejá se é de error
      expect(res.status).to.be.equal(400);
      //verifica se a mensagem é exatamente "Já existe uma conta com esse nome!"
      expect(res.body.error).to.be.equal( 'Já existe uma conta com esse nome!');
  })


})

//iserindo uma movimentação 
it('Movimentação', ()=>{
    cy.verificaID('Conta para movimentacoes')
    .then(contaId =>{
    cy.request({
        url: '/transacoes',
        method: 'POST',
        headers:{ Authorization: `JWT ${token}`},
        body:{
            conta_id: contaId,
            //obs: aqui usamos a data de agora, no tempo presente,
            //porém como é uma conta a ser paga, adicionamos 1 dia a mais, ou seja , 
            //a conta ficará com a data de amanhã
            data_pagamento: Cypress.moment().add({days:1}).format('DD/MM/YYYY'),
            data_transacao: Cypress.moment().format('DD/MM/YYYY'),
            descricao: "desc",
            envolvido: "inter",
            status: true,
            tipo: "REC",
            valor: "123"
        }
    }).as('response')
 })
 //verifica se é o status correto
 cy.get('@response').its('status').should('be.equal', 201)
//verifica se esse ID exist
 cy.get('@response').its('body.id').should('exist')
})

//Teste para verificar se o saldo da conta
it('Saldo', ()=>{
    cy.request({
        url: '/saldo',
        method: 'GET',
        headers:{ Authorization: `JWT ${token}`}
    }).then(res =>{
        let saldoConta = null
        res.body.forEach(c =>{
            if(c.conta === 'Conta para saldo') saldoConta = c.saldo
        })
        expect(saldoConta).to.be.equal('534.00')
    })
    cy.request({
        method: 'GET',
        url: '/transacoes',
       headers:{ Authorization: `JWT ${token}`},
        qs:{ descricao: 'Movimentacao 1, calculo saldo'}
    }).then(res => {
        cy.request({
        url: `/transacoes/${res.body[0].id}`,
        method: 'PUT',
        headers:{ Authorization: `JWT ${token}`},
        body:{
            status: true,
            data_transacao: Cypress.moment(res.body[0].data_transacao).format('DD/MM/YYYY'),
            data_pagamento: Cypress.moment(res.body[0].data_pagamento).format('DD/MM/YYYY'),
            descricao: res.body[0].descricao,
            envolvido: res.body[0].envolvido,
            valor: res.body[0].valor,
            conta_id: res.body[0].conta_id
        }

    }).its('status').should('be.equal', 200)
  })
  cy.request({
    url: '/saldo',
    method: 'GET',
    headers:{ Authorization: `JWT ${token}`}
}).then(res =>{
    let saldoConta = null
    res.body.forEach(c =>{
        if(c.conta === 'Conta para saldo') saldoConta = c.saldo
    })
    expect(saldoConta).to.be.equal('4034.00')
 })
})

//Teste para remover uma movimentação de conta
//agora usando o method: DeleteS
it('Remove uma movimentação', ()=>{
    cy.request({
        method: 'GET',
        url: '/transacoes',
        headers:{ Authorization: `JWT ${token}`},
        qs:{ descricao: 'Movimentacao para exclusao'}
    }).then(res => {
      cy.request({
          url: `/transacoes/${res.body[0].id}`,
          method: 'DELETE',
          headers:{ Authorization: `JWT ${token}`}
        }).its('status').should('be.equal', 204)
    })
 })
 
 
})